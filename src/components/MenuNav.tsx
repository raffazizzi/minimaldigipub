import React, {useState} from 'react'

const MenuNav = ({items, langSwitch, location}) => {
  const [open, setOpen] = useState(false)
  
  return (<>
    <nav className="bg-courseGreen h-10 md:px-20 px-10 text-courseBeige flex uppercase items-center">
      <button className="w-5 md:hidden" onClick={() => setOpen(!open)}>
        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
        </svg>
      </button>
      {
        items.map(item => {
          const active = location === item.link ? 'underline' : ''
          return <a href={item.link} 
          className={`${active} hidden md:inline hover:underline underline-offset-4 decoration-2 mr-6`}>{item.label}</a>
        })
      }

      <a href={langSwitch.link} className="hover:underline decoration-2 underline-offset-4 absolute md:right-20 right-10">{langSwitch.label}</a>  

    </nav>
    <nav className={`${open ? '' : 'hidden'} block pl-10 bg-courseGreen text-courseBeige uppercase`}>
      {
        items.map(item => {
          const active = location === item.link ? 'underline' : ''
          return <a href={item.link} 
          className={`${active} md:hidden block hover:underline underline-offset-4 decoration-2 mr-6 pb-5`}>{item.label}</a>
        })
      }
    </nav>
  </>)
}

export default MenuNav

