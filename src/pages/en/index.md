---
layout: '../../layouts/home.astro'
lang: en
path: /
instructors:
  - name: Dr. Raff Viglianti
    affiliation: (MITH, UMD)
    email: rviglian@umd.edu
  - name: Dr. Gimena del Rio Riande
    affiliation: (USAL, CONICET)
    email: gdelrio@conicet.gov.ar
times:
  - day: Wednesdays 
    time: 12:00pm - 1:50pm
  - day: Fridays 
    time: 10:00 - 10:50pm
location: 0301 A, Hornbake Library 
prerequisites: "None: no coding skills or knowledge of Spanish required."
testudo: https://app.testudo.umd.edu/soc/search?courseId=MITH301&sectionId=&termId=202108&_openSectionsOnly=on&creditCompare=%3E%3D&credits=0.0&courseLevelFilter=ALL&instructor=&_facetoface=on&_blended=on&_online=on&courseStartCompare=&courseStartHour=&courseStartMin=&courseStartAM=&courseEndHour=&courseEndMin=&courseEndAM=&teachingCenter=ALL&_classDay1=on&_classDay2=on&_classDay3=on&_classDay4=on&_classDay5=on
codes: ["MITH 301", "ENGL378M", "CMLT398M", "LASC348C"]
---

In this Global Classrooms course, students of the University of Maryland and Universidad del Salvador, Buenos Aires, will come together to learn how to create and assess websites from a critical and humanities-focused perspective. Specifically, the course will introduce “minimal computing” approaches, which privilege the use of open technologies, ownership of data and code, reduction in computing infrastructure and, consequently, environmental impact.

The course is structured around a group project with students from both universities. You will collaborate virtually to create a multilingual (Spanish and English) digital edition of a colonial era text, while learning about Digital Humanities approaches to literary studies, digital publishing, history, and postcolonial studies.

Knowledge of the Spanish language is not required as teaching and collaboration will be conducted in English. In the spirit of fostering a multilingual approach, however, we will not discourage the use of Spanish among students and instructors; mutual respect and cooperation are paramount to the success of your course project.

After successfully completing this course you will:

* be familiar with the latest debates in the digital publishing of humanities research;
* know the fundamentals of editing primary sources (textual scholarship) for historical and literary research;
* know how to collaborate effectively as part of an international and multilingual team;
* have fundamental skills in managing a digital project;
* know how to create, publish, and maintain a websites with minimal technical and web hosting requirements. 

