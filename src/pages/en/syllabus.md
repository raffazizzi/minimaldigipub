---
layout: '../../layouts/page.astro'
lang: en
path: /syllabus
---

# Course Schedule (2021)

Each week follows a similar model: each Wednesday, we will connect on Zoom with USAL students and introduce a new concept through readings, lectures, and discussion. We will also introduce some practical skills that you can try on your own or, later on, together with your group in breakout rooms. On Fridays, we will review and expand on that week's practical skill.

**Before** each class (starting from week 2) you should write a question or comment about the required readings on ELMS.

All readings not directly linked are available on ELMS. We may present alternatives in the required readings, particularly to offer readings in Spanish (mostly for USAL students, but you can choose to read them if you are proficient). We also list *optional* readings that we may reference in class but that you are not required to read.

## UNIT 1: Digital Humanities

### Week 1 (1.1): Welcome

#### Wed 1 Sept

Introduction to the course

#### Fri 3 Sept

Lab: installing software, learning about files and file types, introduction to VS Code.

### Week 2 (1.2): Why Digital Humanities (DH)?

#### Wed 8 Sept

Required readings (read them and write a question/comment on ELMS-Canvas)

-   The syllabus

-   Burdick, Anne; Johanna Drucker; Peter Lunenfeld; Todd Pressner & Jeffrey Schnapp, J. 2012. *Digital\_Humanities*. Massachusetts: MIT Press, pages 3 and 8-12.\
    OR / AND

-   del Rio Riande, Gimena. "La mirada humana, la mirada crítica". 2020. The Conversation. https://theconversation.com/la-mirada-critica-de-las-humanidades-digitales-131488

Other referenced works (optional)

-   Burdick, Anne; Joanna Drucker; Peter Lunenfeld; Todd Pressner & Jeffrey Schnapp, J. 2012. *Digital\_Humanities*. Massachusetts: MIT Press, pages 3-5 and 122-123.

-   Rojas, Antonio 2017. "Big Data en las Humanidades Digitales". Cultura inteligente: Análisis de tendencias digitales. https://www.accioncultural.es/media/Default%20Files/activ/2017/ebook/anuario/4BigDataHumamidades\_AntonioRojas.pdf

#### Fri 10 Sept

Lab: using Git and GitLab

### Week 3 (1.3): Anatomy of DH projects

You will be assigned to a small group with students from both UMD and USAL.

#### Wed 15 Sept 

Required readings (read them and write a question/comment on ELMS-Canvas)

-   Schmidt, Benjamin. 2012. "Do Digital Humanists need to understand algorithms?". In Gold, Matthew (ed.). *Debates in the Digital Humanities*. Minnesota: University of Minnesota Press. https://dhdebates.gc.cuny.edu/read/untitled/section/557c453b-4abb-48ce-8c38-a77e24d3f0bd\
    **(only first section: "Algorithms and Transforms")**

-   Burdick, Anne; Joanna Drucker; Peter Lunenfeld; Todd Pressner & Jeffrey Schnapp, J. 2012. *Digital\_Humanities*. Massachusetts: MIT Press, **pages 122 - 123.**

Other referenced works (optional)

-   Galina Rusell. 2016. "La evaluación de los recursos digitales para las Humanidades", Revista Signa, vol. 25. http://revistas.uned.es/index.php/signa/article/view/16909

-   Nowviskie, Bethany. 2014. "On the Origin of 'Hack' and 'Yack'" http://journalofdigitalhumanities.org/3-2/on-the-origin-of-hack-and-yack-by-bethany-nowviskie/

#### Fri 17 Sept

Lab: Git review and how to work in groups, communications.

### Week 4 (1.4): Collaboration in DH Projects

#### Wed 22 Sept

Required readings (read them and write a question/comment on ELMS-Canvas)

-   Elements of effective teamwork

<!-- -->

-   Pitti, Daniel V. Designing Sustainable Projects and Publications. 2004. A companion to Digital Humanities. http://www.digitalhumanities.org/companion/view?docId=blackwell/9781405103213/9781405103213.xml&chunk.id=ss1-5-1&toc.depth=1&toc.id=ss1-5-1&brand=default

#### Fri 24 Sept

Lab: group work on Charter due on 09/28

**ASSIGNMENT 1 due by 09/28 11:59pm**

Group charter on the group GitLab repository. Send URL to repository on ELMS.

## UNIT 2: Minimal Computing

### Week 5 (2.1): What is Minimal Computing? Access, bandwidth, ownership

#### Wed 29 Sept

Required readings (read them and write a question/comment on ELMS-Canvas)

-   "About Minimal Computing". Minimal Computing-GO::DH. http://go-dh.github.io/mincomp/about/

-   Gil, Alex. 2015. The User, the Learner and the Machines We Make. Minimal Computing - GO::DH. https://go-dh.github.io/mincomp/thoughts/2015/05/21/user-vs-learner/

Other referenced works (optional)

-   Padilla, M. (2017). Soberanía tecnológica ¿De qué estamos hablando? En A. Haché (ed.), *Soberanía tecnológica, 2*. 3-16. https://www.ritimo.org/IMG/pdf/sobtech2-es-with-covers-web-150dpi-2018-01-13-v2.pdf

#### Fri 01 Oct

Lab: static sites with Gatsby

### Week 6 (2.2): Web publishing fundamentals

#### Wed 06 Oct

Required readings (read them and write a question/comment on ELMS-Canvas)

-   Watch "What is the world wide web?" https://www.youtube.com/watch?v=J8hzJxb0rpc

Other referenced works (optional)

-   Watch "How does the INTERNET work?" https://www.youtube.com/watch?v=x3c1ih2NJEg

#### Fri 08 Oct

Lab: creating and styling pages with HTML and CSS

### Week 7 (2.3): Multilingualism

#### Wed 13 Oct

Required readings (read them and write a question/comment on ELMS-Canvas)

-   Mahony, Simon. 2018. "Cultural diversity and the Digital Humanities". *Fudan Journal of the Humanities and Social Sciences*, vol. 11, pp. 371-388. https://link.springer.com/article/10.1007/s40647-018-0216-0

-   Translation as Research: A Manifesto. Modern Languages Open. DOI: http://doi.org/10.3828/mlo.v0i0.80

Other referenced works (optional)

-   Gil, Alex, & Ortega, Elika. 2016. Global Outlooks in Digital Humanities: Multilingual Practices and Minimal Computing. In Crompton, C. (Ed.) *Doing Digital Humanities Practice, Training, Research*. Zotero

#### Fri 15 Oct

Lab: publishing a website on GitLab pages

**ASSIGNMENT 2 due by 10/19 11:59pm**

The first iteration of the group Gastby site should be on the GitLab repository and public on the web via GitLab pages. Send URL to repository on ELMS.

## UNIT 3: Editing and publishing *An Account of a Voyage up the River de la Plata* by Acarete du Biscay

### Week 8 (3.1): *An Account of a Voyage up the River de la Plata*. Historical source or literary work?

#### Wed 20 Oct

Required readings (read them and write a question/comment on ELMS-Canvas)

-   Du Biscay, Acarete. "A description of the City of Potosi and the mines there". [An Account of a voyage up the river de la Plata, and thence over land to Peru. With observations on the inhabitants, as well Indians and Spaniards; the cities, commerce, fertility, and riches of that part of America.]" Voyages and Discoveries in South America, vol. II, S. Buckley, 1698. World Digital Library, pp. 42-47.

OR / AND

-   Du Biscay, Acarete. "Descripción de la ciudad de Potosí y sus minas". [Relación de un viaje al Río de la Plata y de allí por tierra al Perú con observaciones sobre los habitantes, sean indios o españoles, las ciudades, el comercio, la fertilidad y las riquezas de esta parte de América]. Biblioteca Virtual Miguel de Cervantes, 2001, pp. 67-74.

Other referenced works (optional)

-   Altuna, E. 2004. "Acarette du Biscay: Los vulnerables límites del imperio" Revista de crítica literaria latinoamericana, pp. 189-207. https://as.tufts.edu/romancestudies/rcll/pdfs/60/11-ALTUNA.pdf Acarette du Biscay: Los vulnerables límites del imperio

*Guest lecturer: María Juliana Gandini (IDECU-CONICET): Los viajes de Acarete du Biscay*

#### Fri 22 Oct

Lab: adding a primary source text to your Gastby site with the Text Encoding Initiative (TEI) format

### Week 9 (3.2): What is scholarly editing?

#### Wed 27 Oct

Required readings (read them and write a question/comment on ELMS-Canvas)

-   Driscoll, M. J. 2010. The words on the page: Thoughts on philology, old and new. http://www.driscoll.dk/docs/words.html

#### Fri 29 Oct

Lab: phrase-level text encoding in TEI

**ASSIGNMENT 3 due by 11/02 11:59pm**

The first iteration of the edition in TEI should be added to the Gatsby site on the GitLab repository and public on the web via GitLab pages. Send URL to repository on ELMS. This required assignment is not graded. The tutors will provide feedback and suggestions towards Assignment 4 (graded).

### Week 10 (3.3): What is scholarly editing? Part 2

#### Wed 03 Nov

Required readings (read them and write a question/comment on ELMS-Canvas)

-   Sahle, P. 2016. "What is a Scholarly Digital Edition?" in *Digital Scholarly Editing: Theories and Practices*. Ed. M.J Driscoll and E. Pierazzo. https://books.openedition.org/obp/3397

#### Fri 05 Nov

Lab: adding style and user interaction to your TEI edition with CSS

### Week 11 (3.4): Digital Scholarly Editions

#### Wed 10 Nov

Required readings (read them and write a question/comment on ELMS-Canvas)

-   Greve Rasmussen, K. S. 2016 "Reading or Using a Digital Edition? Reader Roles in Scholarly Editions" in *Digital Scholarly Editing: Theories and Practices*. Ed. M.J Driscoll and E. Pierazzo. https://books.openedition.org/obp/3406

OR / AND

-   Priani Saisó, E. y Guzmán Olmos, A. M. 2014. "TEI como una nueva práctica de lectura. Humanidades Digitales: desafíos, logros y perspectivas de futuro. *Janus*, Anexo 1, 373-382. https://ruc.udc.es/dspace/handle/2183/13567

*Guest lecturer: Antonio Rojas Castro: Sobre ediciones digitales filológicas*

#### Fri 12 Nov

Lab: technical support and group work.

### Week 12 (3.5): Scheduled group work & support

Since this is the last class with USAL students, we may use this class to revise any difficult content or make up for lost time due to unforeseen circumstances. N.B. there is one more class for UMD students in Week 14 before group presentations.

#### Wed 10 Nov

No readings unless we are making up for a lost class.

#### Fri 12 Nov

Lab: technical support and group work.

### Week 13: Thanksgiving break

### Week 14: What does a Global Digital Humanities look like?

#### Wed 01 Dec

Required readings (read them and write a question/comment on ELMS-Canvas)

-   Risam, R. 2019. Excerpt from *New Digital Worlds: Postcolonial Digital Humanities in Theory, Praxis, and Pedagogy.\
    *Introduction: The Postcolonial Digital Cultural Record pp. 3-6\
    Introduction: Postcolonial Digital Humanities beyond Representation pp 13-19\
    Chapter 3: Remaking the Global Worlds of Digital Humanities pp 65-66\
    Chapter 3: The Case for a Digital Humanities Accent pp 79-83

Other referenced works (optional)

-   Fiormonte, D. 2017. Digital Humanities and the Geopolitics of Knowledge. *Digital Studies/Le Champ Numérique*, *7*(1), 5. https://doi.org/10.16995/dscn.274

#### Fri 03 Dec

No class or lab, but we will be available at the usual time for support, questions, etc.

**ASSIGNMENT 4 due by 12/07 11:59pm**

The final version of the group website, including a complete TEI edition of *An Account of a Voyage up the River de la Plata* in both English and Spanish (there will be Spanish native speakers in your group). The code should be on the GitLab repository and the website should be public on the web via GitLab pages. Send URL to repository on ELMS.

**GROUP PRESENTATIONS due on 12/08**

Each group will prepare a 10 minute presentation to illustrate their work to the rest of the class on 12/09.

### Week 15: Group Presentations

#### Wed 08 Dec

Group presentations

#### Fri 10 Dec

We may use this time for any overflow that we might have from the presentations. If we wrap them up on Wednesday, we will not need to hold class on Friday.

**FINAL REFLECTION PAPER due by 12/19 11:59pm**

Upload your final reflection paper (max 3 pages) on ELMS-Canvas as MS Word document or PDF.

### Week 16: Finals

Your final reflection paper is due by 12/19.