---
layout: '../../layouts/home.astro'
lang: es
path: /
instructors:  
  - name: Dr. Gimena del Rio Riande
    affiliation: (USAL, CONICET)
    email: gdelrio@conicet.gov.ar
  - name: Mg. Nidia Hernández 
    affiliation: (USAL, CONICET)
    email: nidiahernandez@conicet.gov.ar
  - name: Romina De León
    affiliation: (CONICET)
  - name: Dr. Raff Viglianti
    affiliation: (MITH, UMD)
    email: rviglian@umd.edu
times:
  - day: Miércoles 
    time: de 13 a 16 horas
prerequisites: No se necesitan conocimientos previos de programación. Este curso es parte de la iniciativa Global Classrooms (USAL-UMD).
---

La edición digital de textos es una tarea cada vez más necesaria para estudiantes de cualquier disciplina. Trabajar con fuentes digitalizadas, hacer una página web son tareas que hacen a la investigación pero que son también aplicables a la vida laboral.

En este curso internacional sobre Humanidades Digitales, el primero en nuestro país, dictado por investigadoras y técnicas del CONICET-UBA y profesores de la University of Maryland (UMD, Estados Unidos), los alumnos aprenderán nociones básicas sobre la edición digital de textos. Para ello, trabajarán desde lo que comúnmente se denomina “Minimal Computing” con los estándares internacionales de la Text Encoding Initiative (TEI) y con tecnologías abiertas como GitHub y Jekyll.

El curso se dictará en formato presencial en español, pero los alumnos interactuarán virtualmente en inglés con docentes y alumnos de la Universidad de Maryland.

Después de una introducción a las Humanidades Digitales, los alumnos trabajarán en la edición digital de un texto cronístico del siglo XVII, la “Descripción de Buenos Ayres”, contenida en Un viaje al Río de la Plata, de Acarete du Biscay. Si bien el trabajo será individual, muchas de las actividades propuestas, como la codificación del texto, serán encaradas en formato grupal.
