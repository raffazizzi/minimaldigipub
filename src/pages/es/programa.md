---
layout: '../../layouts/page.astro'
lang: es
path: /programa
---

# Programa (2021)

Cada semana seguiremos un modelo similar: cada miércoles, nos conectaremos en Zoom con los estudiantes de la Universidad de Maryland e introduciremos un nuevo concepto sobre Humanidades Digitales a través de lecturas, charlas y debates. También realizaremos ejercicios prácticos que luego podrás profundizar en la hora de laboratorio en español. Antes de cada clase (y a partir de la semana 2), deberás escribir una pregunta o comentario en ELMS sobre las lecturas requeridas.

## UNIDAD 1: Humanidades Digitales

### Semana 1 (1.1): Bienvenidxs

#### Mié 1 Sept

Introducción al curso

Lab: repaso de instalación de software, tipos de archivo, introducción a VS Code.

### Semana 2 (1.2): ¿Por qué Humanidades Digitales?

#### Mié 8 Sept

Lecturas (leer y dejar un comentario en ELMS-Canvas):

-   Programa de este curso

-   Burdick, Anne; Johanna Drucker; Peter Lunenfeld; Todd Pressner & Jeffrey Schnapp. 2012. *Digital\_Humanities*. Massachusetts: MIT Press, pages 3 and 8-12.\
    O / Y

-   del Rio Riande, Gimena. "La mirada humana, la mirada crítica". 2020. The Conversation. https://theconversation.com/la-mirada-critica-de-las-humanidades-digitales-131488(https://theconversation.com/la-mirada-critica-de-las-humanidades-digitales-131488)

Opcional:

-   Burdick, Anne; Joanna Drucker; Peter Lunenfeld; Todd Pressner & Jeffrey Schnapp, J. 2012. *Digital\_Humanities*. Massachusetts: MIT Press, pages 3-5 and 122-123.

-   Rojas, Antonio 2017. "Big Data en las Humanidades Digitales". Cultura inteligente: Análisis de tendencias digitales. https://www.accioncultural.es/media/Default%20Files/activ/2017/ebook/anuario/4BigDataHumamidades\_AntonioRojas.pdf(https://www.accioncultural.es/media/Default%20Files/activ/2017/ebook/anuario/4BigDataHumamidades_AntonioRojas.pdf)

Lab: uso de Git y GitLab.

### Semana 3 (1.3): Características de un proyecto de Humanidades Digitales

Formaremos pequeños grupos entre UMD y USAL.

#### Mié 15 Sept 

Lecturas (leer y dejar un comentario en ELMS-Canvas):

-   Schmidt, Benjamin. 2012. "Do Digital Humanists need to understand algorithms?". In Gold, Matthew (ed.). *Debates in the Digital Humanities*. Minnesota: University of Minnesota Press. **(only first section: "Algorithms and Transforms")\
    **https://dhdebates.gc.cuny.edu/read/untitled/section/557c453b-4abb-48ce-8c38-a77e24d3f0bd(https://dhdebates.gc.cuny.edu/read/untitled/section/557c453b-4abb-48ce-8c38-a77e24d3f0bd)

-   Burdick, Anne; Joanna Drucker; Peter Lunenfeld; Todd Pressner & Jeffrey Schnapp, J. 2012. *Digital\_Humanities*. Massachusetts: MIT Press, pages SG4 and 5

Opcional:

-   Galina Rusell. 2016. "La evaluación de los recursos digitales para las Humanidades", Revista Signa, vol. 25. http://revistas.uned.es/index.php/signa/article/view/16909(http://revistas.uned.es/index.php/signa/article/view/16909)

Lab: revisión de uso de Git y de cómo trabajar en grupo.

### Semana 4 (1.4): La importancia de la colaboración en los proyectos de Humanidades Digitales

#### Mié 22 Sept

Lecturas (leer y dejar un comentario en ELMS-Canvas):

-   Elementos para una comunicación eficaz

-   Pitti, Daniel V. Designing Sustainable Projects and Publications. 2004. A companion to Digital Humanities. http://www.digitalhumanities.org/companion/view?docId=blackwell/9781405103213/9781405103213.xml&chunk.id=ss1-5-1&toc.depth=1&toc.id=ss1-5-1&brand=default(http://www.digitalhumanities.org/companion/view?docId=blackwell/9781405103213/9781405103213.xml&chunk.id=ss1-5-1&toc.depth=1&toc.id=ss1-5-1&brand=default)

Lab: Trabajo en grupo para la escritura del Charter (Contrato del equipo) que se entregará el **28/09**

**TAREA 1 - Entrega 28/9 11:59pm**

Group Charter en el repositorio grupal de GitLab. Se enviará la URL de GitLab a ELMS.

## UNIDAD 2: Minimal Computing

### Semana 5 (2.1): ¿Qué es la Minimal Computing? Acceso, ancho de banda y propiedad

#### Mié 30 Sept

Lecturas (leer y dejar un comentario en ELMS-Canvas):

-   "About Minimal Computing". Minimal Computing-GO::DH. http://go-dh.github.io/mincomp/about/(http://go-dh.github.io/mincomp/about/)

-   Gil, Alex. 2015. The User, the Learner and the Machines We Make. Minimal Computing - GO::DH. https://go-dh.github.io/mincomp/thoughts/2015/05/21/user-vs-learner/(https://go-dh.github.io/mincomp/thoughts/2015/05/21/user-vs-learner/)

Optativo:

Padilla, M. (2017). Soberanía tecnológica ¿De qué estamos hablando? En A. Haché (ed.), *Soberanía tecnológica, 2*. 3-16. https://www.ritimo.org/IMG/pdf/sobtech2-es-with-covers-web-150dpi-2018-01-13-v2.pdf(https://www.ritimo.org/IMG/pdf/sobtech2-es-with-covers-web-150dpi-2018-01-13-v2.pdf)

Lab: Sitios estáticos con Gatsby

### Semana 6 (2.2): Bases para la publicación web

#### Mié 6 Oct

Lecturas (mirar y dejar un comentario en ELMS-Canvas):

-   Video: "What is the world wide web?" https://www.youtube.com/watch?v=J8hzJxb0rpc(https://www.youtube.com/watch?v=J8hzJxb0rpc)

Opcional:

-   Video: "How does the Internet work?" https://www.youtube.com/watch?v=x3c1ih2NJEg(https://www.youtube.com/watch?v=x3c1ih2NJEg)

Lab: Creación y estilo de páginas con HTML and CSS

### Semana 7 (2.3): Multilingüismo

#### Mié 13 Oct

Lecturas (leer y dejar un comentario en ELMS-Canvas):

-   Mahony, Simon. 2018. "Cultural diversity and the Digital Humanities". [*Fudan Journal of the Humanities and Social Sciences*](https://link.springer.com/journal/40647), vol. 11, pp. 371-388. https://link.springer.com/article/10.1007/s40647-018-0216-0(https://link.springer.com/article/10.1007/s40647-018-0216-0)

-   Translation as Research: A Manifesto. Modern Languages Open. DOI: http://doi.org/10.3828/mlo.v0i0.80(http://doi.org/10.3828/mlo.v0i0.80)

Opcional:

-   Gil, Alex, & Ortega, Elika. (2016). Global Outlooks in Digital Humanities: Multilingual Practices and Minimal Computing. In Crompton, C. (Ed.) *Doing Digital Humanities Practice, Training, Research*. Zotero

Lab: Publicar una web en GitLab pages

**TAREA 2 - Entrega 19/10 11:59pm**

El sitio de Gatsby deberá estar en el repositorio de GitLab y público en la web vía GitLab pages. Enviar la URL a ELMS.

## UNIDAD 3: Edición y publicación de *Relación de un viaje al Río de la Plata*/ *An Account of a Voyage up the River de la Plata* de Acarete du Biscay

### Semana 8 (3.1): *Relación de un viaje al Río de la Plata*/ *An Account of a Voyage up the River de la Plata* de Acarete du Biscay. ¿Relato histórico o literario?

#### Mié 20 Oct

Lecturas (leer y dejar un comentario en ELMS-Canvas):

-   Du Biscay, Acarete. "A description of the City of Potosi and the mines there". [An Account of a voyage up the river de la Plata, and thence over land to Peru. With observations on the inhabitants, as well Indians and Spaniards; the cities, commerce, fertility, and riches of that part of America.](https://www.wdl.org/es/item/235/view/1/1/)" Voyages and Discoveries in South America, vol. II, S. Buckley, 1698. World Digital Library, pp. 42-47.

O

-   Du Biscay, Acarete. "Descripción de la ciudad de Potosí y sus minas". [Relación de un viaje al Río de la Plata y de allí por tierra al Perú con observaciones sobre los habitantes, sean indios o españoles, las ciudades, el comercio, la fertilidad y las riquezas de esta parte de América](http://www.cervantesvirtual.com/nd/ark:/59851/bmcw9556). Biblioteca Virtual Miguel de Cervantes, 2001, pp. 67-74.

Opcional:

-   Altuna, Elena. 2004. "[Acarette du Biscay: Los vulnerables límites del imperio](https://as.tufts.edu/romancestudies/rcll/pdfs/60/11-ALTUNA.pdf)" Revista de crítica literaria latinoamericana, pp. 189-207. https://as.tufts.edu/romancestudies/rcll/pdfs/60/11-ALTUNA.pdf(https://as.tufts.edu/romancestudies/rcll/pdfs/60/11-ALTUNA.pdf)

Invitada especial: María Juliana Gandini (IDECU-CONICET): Los viajes de Acarete du Biscay

Lab: Añadir una fuente primaria en formato TEI (Text Encoding Initiative ) en tu sitio Gatsby.

### Semana 9 (3.2): ¿Qué es la edición académica de textos?

#### Mié 27 Oct

Lecturas (leer y dejar un comentario en ELMS-Canvas):

-   Driscoll, M. J. 2010. The words on the page: Thoughts on philology, old and new. http://www.driscoll.dk/docs/words.html(http://www.driscoll.dk/docs/words.html)

Lab: Codificación de textos en TEI

**TAREA 3 - Entrega 2/11 11:59pm**

La edición en TEI será añadida al sitio en Gatsby vía repositorio de GitLab y se publicará en la web vía GitLab pages. Enviar URL a ELMS.

### Semana 10 (3.3): ¿Qué es la edición académica? Parte 2

#### Mié 3 Nov

Lecturas (leer y dejar un comentario en ELMS-Canvas):

-   Sahle, P. 2016. "What is a Scholarly Digital Edition?" in *Digital Scholarly Editing: Theories and Practices*. Ed. M.J Driscoll and E. Pierazzo. https://books.openedition.org/obp/3397(https://books.openedition.org/obp/3397)

Lab: Añadir estilo e interacción con el usuario en tu edición TEI con CSS.

### Semana 11 (3.4): Ediciones digitales académicas

#### Mié 10 Nov

Lecturas (leer y dejar un comentario en ELMS-Canvas):

-   Greve Rasmussen, K. S. 2016 "Reading or Using a Digital Edition? Reader Roles in Scholarly Editions" in *Digital Scholarly Editing: Theories and Practices*. Ed. M.J Driscoll and E. Pierazzo. https://books.openedition.org/obp/3406(https://books.openedition.org/obp/3406)

O

-   Priani Saisó, E. y Guzmán Olmos, A. M. 2014. "TEI como una nueva práctica de lectura. Humanidades Digitales: desafíos, logros y perspectivas de futuro. *Janus*, Anexo 1, 373-382. https://ruc.udc.es/dspace/handle/2183/13567(https://ruc.udc.es/dspace/handle/2183/13567)

Invitado especial: Antonio Rojas Castro: Sobre ediciones digitales filológicas

Lab: Soporte técnico y trabajo en grupo.

### Semana 12 (3.5): Revisión o refuerzo de lo aprendido en el curso.

#### Mié 17 Nov

Trabajo en grupo

**TAREA 4 - Entrega final 7/12 11:59pm**

Versión final del sitio web del grupo, que incluye una edición TEI completa del texto de Acarete (inglés y español). El código debe estar en el repositorio de GitLab y el sitio web debe estar público en la web a través de GitLab pages. Enviar la URL a ELMS.

**PRESENTACIONES GRUPALES el 8/12**

Cada grupo presentará su edición el 8/12.
