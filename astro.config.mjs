// @ts-check
export default /** @type {import('astro').AstroUserConfig} */ ({
  public: 'static',
  dist: 'public',
  buildOptions: {
    site: 'https://mith.umd.edu/minimaldigipub/',
    sitemap: true,
  },
	renderers: ['@astrojs/renderer-react'],
});
