module.exports = {
	content: [
		'./public/**/*.html',
		'./src/**/*.{astro,html,js,jsx,ts,tsx}'],
	theme: {
		fontFamily: {
			'sans': ['Lato', 'sans-serif'],
			'serif': ['EB Garamond', 'serif']
		},
		colors: {
      'courseBrown': '#4f3d29',
			'courseGreen': '#1f8464',
			'courseBeige': '#fbecd5',
			'gray': '#F7FAFC',
			'white': '#fff',
    },
		extend: {
      padding: {
        '1/2': '50%',
        full: '100%',
      },
    },
	},
	plugins: [
    require('@tailwindcss/typography'),
  ],
};
